import logging
from datetime import datetime

from flask import url_for, jsonify, request
from werkzeug.utils import redirect

from polo.app_exceptions import NotFound, BadRequest
from polo.helpers import remove_intraday
from polo.model import Expedicao
from polo.repository.db import DB
from polo.repository.sqlalchemy_repository import PoloSqlAlchemyRepository


def boas_vindas():
    return jsonify({
        'mensagem': 'Bem-vindo à API',
        'uri': url_for('.lista_polos', _external=True)
    })


def lista_polos():
    session = DB.get_session()
    repo = PoloSqlAlchemyRepository(session)
    lista_polos = repo.list()
    return jsonify({'polos': [
        {'nome': polo.nome,
         'id_polo': polo.id_polo,
         'cobertura': polo.cobertura,
         'criticidade': polo.criticidade,
         'uri': url_for('.detalha_polo', id_polo=polo.id_polo, _external=True)}
        for polo in lista_polos]})


def detalha_polo(id_polo):
    session = DB.get_session()
    repo = PoloSqlAlchemyRepository(session)
    polo = repo.get(id_polo)

    if not polo:
        raise NotFound(f'O polo {id_polo} não pode ser encontrado', payload={'path': request.path})

    return jsonify(
        {'nome': polo.nome,
         'id_polo': str(polo.id_polo),
         'cobertura': polo.cobertura,
         'criticidade': polo.criticidade,
         'terminais_disponiveis': polo.terminais_disponiveis,
         'media_atendimentos': round(polo.media, 2),
         'ordens_de_servico': [ordem.data_atendimento.date().isoformat() for ordem in
                               polo._ordens_executadas],
         'expedicoes': [
             {'quantidade': exp.num_terminais,
              'data': exp.data_expedicao.date().isoformat()} for exp in polo._terminais_recebidos]})


def expede_aparelhos(id_polo):
    data = request.get_json()

    if not 'quantidade' in data.keys() or type(data.get('quantidade')) != int or data.get('quantidade') <= 0:
        raise BadRequest('Formato incorreto. O payload tem que seguir o formato {"quantidade": <inteiro positivo>}',
                         payload={'path': request.path, 'payload': data})

    session = DB.get_session()
    repo = PoloSqlAlchemyRepository(session)
    polo = repo.get(id_polo)
    polo.terminais_recebidos(Expedicao(data.get('quantidade'), data_expedicao=remove_intraday(datetime.today())))
    repo.add(polo)
    return {
        'expedicao': data,
        'polo': {'nome': polo.nome,
                 'id_polo': str(polo.id_polo),
                 'cobertura': polo.cobertura,
                 'criticidade': polo.criticidade,
                 'terminais_disponiveis': polo.terminais_disponiveis,
                 'media_atendimentos': round(polo.media, 2),
                 'ordens_de_servico': [ordem.data_atendimento.date().isoformat() for ordem in
                                       polo._ordens_executadas],
                 'expedicoes': [
                     {'quantidade': exp.num_terminais,
                      'data': exp.data_expedicao.date().isoformat()} for exp in polo._terminais_recebidos]}}
