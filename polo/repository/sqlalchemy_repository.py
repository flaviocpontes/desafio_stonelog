from uuid import UUID

from polo.model import Polo


class PoloSqlAlchemyRepository:

    def __init__(self, session):
        self.session = session

    def add(self, polo: Polo):
        self.session.add(polo)
        self.session.commit()

    def get(self, id_polo: UUID):
        return self.session.query(Polo).get(id_polo)

    def list(self):
        return sorted(self.session.query(Polo).all())
