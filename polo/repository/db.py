import json
import logging

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class DB:
    engine = None
    get_session = None

    @classmethod
    def init_db(cls, config):
        cls.engine = create_engine_from_config(config)
        cls.get_session = sessionmaker(bind=cls.engine)


def create_engine_from_config(config):
    logging.info(f'Configurando API. Ambiente de {"TESTE" if config.TESTING else "PRODUÇÃO"}')
    if config.TESTING:
        return create_engine(config.DATABASE_URI)
    logging.info(json.dumps({
        'driver': config.DB_DRIVER_NAME,
        'host': config.DB_HOST,
        'database': config.DB_NAME,
        'user': config.DB_USER,
        'connection_name': config.CONNECTION_NAME
    }))
    return create_engine(
        sqlalchemy.engine.url.URL(
            drivername=config.DB_DRIVER_NAME,
            username=config.DB_USER,
            host=config.DB_HOST,
            password=config.DB_PASSWORD,
            database=config.DB_NAME,
            query=config.DB_QUERY if config.CONNECTION_NAME else None
        ),
        pool_size=5,
        max_overflow=2,
        pool_timeout=30,
        pool_recycle=1800)
