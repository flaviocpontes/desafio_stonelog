from datetime import datetime


def remove_intraday(d: datetime) -> datetime:
    new_d = datetime(d.year, d.month, d.day)
    return new_d
