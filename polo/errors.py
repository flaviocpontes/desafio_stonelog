import logging

from flask import jsonify


def nao_encontrado(error):
    resp = jsonify(error.to_dict())
    resp.status_code = error.status_code
    logging.info(error.to_dict)
    return resp


def requisicao_malformada(error):
    resp = jsonify(error.to_dict())
    resp.status_code = error.status_code
    return resp
