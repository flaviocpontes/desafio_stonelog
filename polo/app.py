from flask import Flask

from polo.app_exceptions import NotFound, BadRequest
from polo.errors import nao_encontrado, requisicao_malformada
from polo.repository import orm
from polo.repository.db import DB
from polo.views import boas_vindas, lista_polos, detalha_polo, expede_aparelhos


def create_app(config_obj):
    app = Flask(__name__)
    config = config_obj()
    app.config.from_object(config)

    orm.start_mappers()
    DB.init_db(config)

    app.register_error_handler(BadRequest, requisicao_malformada)
    app.register_error_handler(NotFound, nao_encontrado)

    app.add_url_rule('/', view_func=boas_vindas, methods=['GET'])
    app.add_url_rule(f'{app.config["API_ROOT"]}/polos', view_func=lista_polos, methods=['GET'])
    app.add_url_rule(f'{app.config["API_ROOT"]}/polos/<uuid:id_polo>', view_func=detalha_polo, methods=['GET'])
    app.add_url_rule(f'{app.config["API_ROOT"]}/polos/<uuid:id_polo>/expedicao', view_func=expede_aparelhos,
                     methods=['POST'])
    return app
