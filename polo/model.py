from dataclasses import dataclass
from datetime import datetime
from typing import List
from uuid import UUID, uuid4

import math

from polo.helpers import remove_intraday


class EstoqueInsuficiente(Exception):
    pass


@dataclass()
class Expedicao:
    '''Envio de terminais para o Polo'''
    num_terminais: int
    data_expedicao: datetime


@dataclass()
class OrdemDeServico:
    '''Uma ordem de serviço representando um atendimento do Green Angel'''
    data_atendimento: datetime


class Polo:
    '''Centro de trabalho dos Green Angels'''

    def __init__(self,
                 nome: str,
                 id_polo: UUID = None,
                 expedicoes: List[Expedicao] = None,
                 ordens_de_servico: List[OrdemDeServico] = None,
                 cobertura: int = None):

        if id_polo is None:
            id_polo = uuid4()
        self.id_polo = id_polo
        self.nome = nome
        self._terminais_recebidos: List[Expedicao] = expedicoes or list()
        self._ordens_executadas: List[OrdemDeServico] = ordens_de_servico or list()

        self._cobertura = cobertura
        if cobertura is None:
            self._calcula_cobertura()
        self.media: int = 0

    def __repr__(self):
        return f'Polo({repr(self.nome)}, {repr(self.id_polo)},' \
               f' [{str.join(", ", [repr(exp) for exp in self._terminais_recebidos])}],' \
               f' [{str.join(", ", [repr(os) for os in self._ordens_executadas])}], {repr(self._cobertura)})'

    def __eq__(self, other):
        if not isinstance(other, Polo):
            return False
        return (other.nome, other.id_polo) == (self.nome, self.id_polo)

    def __gt__(self, other):
        return self.cobertura > other.cobertura

    def _calcula_cobertura(self):
        '''Atualiza quanto o estoque atual deve durar baseado na média de atendimentos diarios'''
        if len(self._ordens_executadas) == 0:
            self._cobertura = 0
            return

        dias_intervalo = (remove_intraday(datetime.today()) - remove_intraday(
            self._ordens_executadas[0].data_atendimento)).days
        if dias_intervalo == 0:
            self._cobertura = 0
            return

        if dias_intervalo < 0:
            raise RuntimeError(
                f'Cálculo de data/hora comprometido ou dados de execução de Ordens de Serviço inconsistente.'
                f' Intervalo entre a data atual e o primeiro atendimento não pode ser {dias_intervalo}')
        num_atendimentos = len(self._ordens_executadas)
        self.media = num_atendimentos / dias_intervalo if dias_intervalo > 0 else 0
        unidades_recebidas = sum([exp.num_terminais for exp in self._terminais_recebidos])
        estoque_atual = unidades_recebidas - num_atendimentos
        self._cobertura = math.floor(estoque_atual // self.media)

    @property
    def terminais_disponiveis(self) -> int:
        num_atendimentos = len(self._ordens_executadas)
        unidades_recebidas = sum([exp.num_terminais for exp in self._terminais_recebidos])
        return unidades_recebidas - num_atendimentos

    @property
    def cobertura(self) -> int:
        self._calcula_cobertura()
        return self._cobertura

    @cobertura.setter
    def cobertura(self, cob: int):
        self._cobertura = cob
        self._calcula_cobertura()

    @property
    def criticidade(self) -> str:
        '''Retorna a criticidade da cobertura'''
        if self.cobertura < 10 or self.cobertura > 23:
            return 'VERMELHA'
        if 13 >= self.cobertura >= 10 or 19 <= self.cobertura <= 23:
            return 'AMARELA'
        if 14 <= self.cobertura <= 18:
            return 'VERDE'

    def terminais_recebidos(self, expedicao: Expedicao):
        '''Evento do recebimento de uma expedição de terminais'''
        self._terminais_recebidos.append(expedicao)
        self._calcula_cobertura()

    def os_executada(self, ordem: OrdemDeServico):
        '''Evento da execução de uma ordem de serviço'''
        if self.terminais_disponiveis < 1:
            raise EstoqueInsuficiente
        self._ordens_executadas.append(ordem)
        self._calcula_cobertura()
