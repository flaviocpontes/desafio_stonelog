# Desafio Stone Log

[![ggiitlab-ci](https://gitlab.com/flaviocpontes/desafio_stonelog/badges/master/pipeline.svg)](https://gitlab.com/flaviocpontes/desafio_stonelog)
[![codecov](https://codecov.io/gl/flaviocpontes/desafio_stonelog/branch/master/graph/badge.svg)](https://codecov.io/gl/flaviocpontes/desafio_stonelog)

Esta é a minha solução para o desafio proposto pela StoneLog para a seleção.

# Instalação

### Localmente

Para instalar a aplicação é preciso ter o python 3.8.4 instalado e rodar o seguinte comando: `pip install -r requirements-dev.txt`

### Docker Compose

Para iniciar a aplicação via docker, basta construir a imagem do Docker usando o comando: `docker-compose up .` e rodar o script da carga inicial no Banco de Dados com o comando `python -m scripts.populate_database` 

### Desenvolvimento

Para levantar uma instância do banco de dados basta rodar o docker-compose com o seguinte comando: 
`docker-compose up db`.
Em seguida é preciso rodar o seguinte comando: `python -m scripts/populate_database`

### Testes

Os testes são iniciados pelo comando `pytest --cov=polo tests/tests*.py` 

### Uso

Atualmenta a api de produção se encontra em: https://desafio-stonelog-gnsmsmhutq-uc.a.run.app/

A API tem os seguintes endpoints:
- `GET /api/polos`: Lista os polos cadastrados com as informações mais importantes e em ordem crescente de cobertura, de forma que os polos mais carentes de uma expedição sejam prioritários. Segue um exemplo.
>- Comando: `curl --location https://desafio-stonelog-gnsmsmhutq-uc.a.run.app/`
```json
{
  "polos": [
    {
      "cobertura": 1,
      "criticidade": "VERMELHA",
      "id_polo": "b4ba8672-de1d-4aa0-8a0f-bc692d344b07",
      "nome": "Recife",
      "path": "/api/polos/b4ba8672-de1d-4aa0-8a0f-bc692d344b07"
    },
    {
      "cobertura": 3,
      "criticidade": "VERMELHA",
      "id_polo": "02b7c948-c862-4e57-aa4c-f61852676173",
      "nome": "Belo Horizonte",
      "path": "/api/polos/02b7c948-c862-4e57-aa4c-f61852676173"
    },
    {
      "cobertura": 13,
      "criticidade": "AMARELA",
      "id_polo": "5ed107b5-0d3e-48a6-a9c5-d9145cef0156",
      "nome": "Manaus",
      "path": "/api/polos/5ed107b5-0d3e-48a6-a9c5-d9145cef0156"
    },
    {
      "cobertura": 16,
      "criticidade": "VERDE",
      "id_polo": "64a204c6-2d3e-4a05-be2b-808618f97d8d",
      "nome": "Bel\u00e9m",
      "path": "/api/polos/64a204c6-2d3e-4a05-be2b-808618f97d8d"
    },
    {
      "cobertura": 26,
      "criticidade": "VERMELHA",
      "id_polo": "dd405ee8-0347-4381-b771-c4110f66f567",
      "nome": "S\u00e3o Paulo",
      "path": "/api/polos/dd405ee8-0347-4381-b771-c4110f66f567"
    }
  ]
}
```
- `GET /api/polos/<id_polo>`: Detalha um polo específico, conforme exemplo a seguir.
>- Comando: `curl https://desafio-stonelog-gnsmsmhutq-uc.a.run.app/api/polos/<id_polo>`
```json
{
  "cobertura": 13,
  "criticidade": "AMARELA",
  "expedicoes": [
    {
      "data": "2020-07-08",
      "quantidade": 15
    },
    {
      "data": "2020-07-13",
      "quantidade": 10
    }
  ],
  "id_polo": "5ed107b5-0d3e-48a6-a9c5-d9145cef0156",
  "nome": "Manaus",
  "ordens_de_servico": [
    "2020-07-08",
    "2020-07-08",
    "2020-07-08",
    "2020-07-09",
    "2020-07-09",
    "2020-07-10",
    "2020-07-11",
    "2020-07-11",
    "2020-07-12",
    "2020-07-18",
    "2020-07-20",
    "2020-07-21",
    "2020-07-21"
  ],
  "terminais_disponiveis": 12
}
```
- `POST /api/polos/<id_polo>/expedicao`: Envia terminais para o polo. O payload deve ser como a seguir:
>- Comando: `curl -X POST -H "Content-Type: application/json" -d '{"quantidade": 4}' https://desafio-stonelog-gnsmsmhutq-uc.a.run.app/api/polos/<id_polo>/expedicao`
```json
{
  "expedicao": {
    "quantidade": 4
  },
  "polo": {
    "cobertura": 13,
    "criticidade": "AMARELA",
    "expedicoes": [
      {
        "data": "2020-07-08",
        "quantidade": 15
      },
      {
        "data": "2020-07-13",
        "quantidade": 10
      }
    ],
    "id_polo": "5ed107b5-0d3e-48a6-a9c5-d9145cef0156",
    "nome": "Manaus",
    "ordens_de_servico": [
      "2020-07-08",
      "2020-07-08",
      "2020-07-08",
      "2020-07-09",
      "2020-07-09",
      "2020-07-10",
      "2020-07-11",
      "2020-07-11",
      "2020-07-12",
      "2020-07-18",
      "2020-07-20",
      "2020-07-21",
      "2020-07-21"
    ],
    "terminais_disponiveis": 12
  }
}
```