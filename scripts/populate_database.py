from datetime import datetime, date, timedelta

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import ProductionConfig
from polo.model import Polo, Expedicao, OrdemDeServico
from polo.repository.orm import metadata, start_mappers


def gera_expedicoes(quant_dias: list) -> list:
    return [Expedicao(quant, hoje - timedelta(days=dias_atras)) for (quant, dias_atras) in quant_dias]


def gera_ordens_de_servico(dias: list) -> list:
    return [OrdemDeServico(hoje - timedelta(days=dias_atras)) for dias_atras in dias]


config = ProductionConfig()

engine = create_engine(
    sqlalchemy.engine.url.URL(
        drivername=config.DB_DRIVER_NAME,
        username=config.DB_USER,
        host=config.DB_HOST,
        password=config.DB_PASSWORD,
        database=config.DB_NAME,
        query=None
    ),
    pool_size=5,
    max_overflow=2,
    pool_timeout=30,
    pool_recycle=1800)
metadata.drop_all(engine)
metadata.create_all(engine)
start_mappers()
session = sessionmaker(bind=engine)()

hoje = datetime.fromisoformat(date.today().isoformat())
polo_manaus = Polo('Manaus',
                   expedicoes=gera_expedicoes([(15, 15), (10, 10)]),
                   ordens_de_servico=gera_ordens_de_servico([15, 15, 15, 14, 14, 13, 12, 12, 11, 5, 3, 2, 2]))
polo_belem = Polo('Belém',
                  expedicoes=gera_expedicoes([(15, 15), (12, 10)]),
                  ordens_de_servico=gera_ordens_de_servico([15, 15, 15, 14, 14, 13, 12, 12, 11, 5, 3, 2, 2]))
polo_recife = Polo('Recife',
                   expedicoes=gera_expedicoes([(30, 30), (15, 20), (15, 10)]),
                   ordens_de_servico=gera_ordens_de_servico(
                       [30, 30, 30, 29, 29, 29, 29, 28, 28, 28, 28, 26, 26, 25, 25, 24, 24, 24, 24, 22, 22, 22, 18, 18,
                        18, 18, 18, 17, 16, 16, 16, 16, 15, 14, 14, 14, 11, 10, 10, 10, 9, 9, 9, 9, 8, 7, 7, 6, 6, 6, 5,
                        4, 4, 4, 3, 2, 1]))
polo_sao_paulo = Polo('São Paulo',
                      expedicoes=gera_expedicoes(
                          [(45, 60), (45, 55), (30, 50), (30, 45), (30, 40), (30, 35), (30, 30), (30, 10)]),
                      ordens_de_servico=gera_ordens_de_servico(
                          [60, 60, 60, 59, 59, 58, 58, 58, 58, 57, 57, 57, 57, 57, 57, 56, 56, 56, 55, 55, 55, 55,
                           54, 54, 54, 54, 54, 53, 53, 53, 53, 52, 52, 52, 51, 51, 51, 51, 51, 51, 51, 49, 49, 49,
                           48, 48, 48, 48, 48, 47, 47, 47, 47, 47, 47, 47, 47, 46, 46, 46, 46, 46, 46, 46, 46, 45,
                           45, 45, 45, 45, 45, 43, 43, 43, 43, 43, 43, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 41,
                           41, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 39, 39, 39, 38, 38, 38, 38, 38, 38,
                           37, 37, 37, 37, 37, 37, 36, 36, 36, 36, 36, 35, 35, 35, 34, 34, 34, 33, 33, 33, 32, 31,
                           30, 30, 30, 29, 29, 29, 29, 28, 28, 28, 28, 26, 26, 25, 25, 24, 24, 24, 24, 22, 22, 22,
                           18, 18, 18, 18, 18, 17, 16, 16, 16, 16, 15, 14, 14, 14, 11, 10, 10, 10, 9, 9, 9, 9, 8,
                           7, 7, 6, 6, 6, 5, 4, 4, 4, 3, 2]))
polo_belo_horizonte = Polo('Belo Horizonte',
                           expedicoes=gera_expedicoes([(35, 50), (20, 40), (20, 30), (20, 10)]),
                           ordens_de_servico=gera_ordens_de_servico(
                               [49, 49, 48, 48, 48, 47, 47, 47, 47, 46, 46, 46, 46, 46, 45, 45, 45, 45, 45, 45, 43,
                                43, 42, 42, 42, 42, 41, 41, 40, 40, 40, 40, 39, 39, 38, 38, 38, 37, 36, 36, 35, 34,
                                33, 32, 31, 30, 30, 30, 29, 28, 26, 25, 24, 24, 24, 24, 22, 22, 22, 18, 18, 18, 18,
                                18, 17, 16, 16, 16, 16, 15, 14, 14, 14, 11, 10, 10, 9, 9, 8, 7, 6, 6, 5, 4, 4, 3, 2,
                                1]))
session.add(polo_manaus)
session.add(polo_belem)
session.add(polo_recife)
session.add(polo_sao_paulo)
session.add(polo_belo_horizonte)
session.commit()
