from datetime import datetime
from uuid import uuid4

from freezegun import freeze_time

from polo.model import Polo, Expedicao, OrdemDeServico
from polo.repository.orm import polos
from polo.repository.sqlalchemy_repository import PoloSqlAlchemyRepository


def test_repositorio_consegue_persistir(in_memory_session):
    repo = PoloSqlAlchemyRepository(in_memory_session)
    polo = Polo('Novo Polo')

    repo.add(polo)
    polo2 = Polo('Novíssimo Polo')
    repo.add(polo2)

    polo_recuperado = in_memory_session.query(Polo).get(polo.id_polo)
    assert polo == polo_recuperado
    polo_recuperado2 = in_memory_session.query(Polo).get(polo2.id_polo)
    assert polo2 == polo_recuperado2


def test_repositorio_consegue_recuperar(in_memory_session):
    id_polo = uuid4()
    in_memory_session.execute(polos.insert(), {'id_polo': id_polo, 'nome': 'Polo Vazio', 'cobertura': 0})
    in_memory_session.commit()
    id_polo2 = uuid4()
    in_memory_session.execute(polos.insert(), {'id_polo': id_polo2, 'nome': 'Polo Dois', 'cobertura': 200})
    in_memory_session.commit()

    repo = PoloSqlAlchemyRepository(in_memory_session)

    polo_recuperado = repo.get(id_polo)
    assert id_polo == polo_recuperado.id_polo
    polo_recuperado = repo.get(id_polo2)
    assert id_polo2 == polo_recuperado.id_polo


@freeze_time("2015-05-8")
def test_repositorio_lista_ordenada_corretamente(in_memory_session):
    polo1 = Polo('Menor Cobertura',
                 expedicoes=[Expedicao(2, datetime(2015, 5, 5)),
                             Expedicao(2, datetime(2015, 5, 6))],
                 ordens_de_servico=[OrdemDeServico(datetime(2015, 5, 5)),
                                    OrdemDeServico(datetime(2015, 5, 6)),
                                    OrdemDeServico(datetime(2015, 5, 7))])
    polo2 = Polo('Maior Cobertura',
                 expedicoes=[Expedicao(4, datetime(2015, 5, 5)),
                             Expedicao(4, datetime(2015, 5, 6)),
                             Expedicao(4, datetime(2015, 5, 7))],
                 ordens_de_servico=[OrdemDeServico(datetime(2015, 5, 5)),
                                    OrdemDeServico(datetime(2015, 5, 6)),
                                    OrdemDeServico(datetime(2015, 5, 7))])
    polo3 = Polo('Media Cobertura',
                 expedicoes=[Expedicao(4, datetime(2015, 5, 5)),
                             Expedicao(2, datetime(2015, 5, 6)),
                             Expedicao(2, datetime(2015, 5, 7))],
                 ordens_de_servico=[OrdemDeServico(datetime(2015, 5, 5)),
                                    OrdemDeServico(datetime(2015, 5, 6)),
                                    OrdemDeServico(datetime(2015, 5, 7))])
    repo = PoloSqlAlchemyRepository(in_memory_session)
    repo.add(polo2)
    repo.add(polo1)
    repo.add(polo3)

    expected = [polo1, polo3, polo2]
    assert expected == repo.list()
