import os
from datetime import datetime, date, timedelta

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, clear_mappers

from polo.model import Polo, Expedicao, OrdemDeServico
from polo.repository.orm import metadata, start_mappers


@pytest.fixture
def in_memory_db():
    engine = create_engine('sqlite:///:memory:')
    metadata.create_all(engine)
    return engine


@pytest.fixture
def in_memory_session(in_memory_db):
    start_mappers()
    yield sessionmaker(bind=in_memory_db)()
    clear_mappers()


@pytest.fixture
def test_db():
    engine = create_engine('sqlite:///test_database.sqlite')
    metadata.create_all(engine)
    return engine


@pytest.fixture
def test_session(test_db):
    start_mappers()
    yield sessionmaker(bind=test_db)()
    clear_mappers()


@pytest.fixture
def mock_data(test_session):
    hoje = datetime.fromisoformat(date.today().isoformat())
    polo_manaus = Polo('Manaus',
                       expedicoes=[Expedicao(quant, hoje - timedelta(days=dias_atras)) for (quant, dias_atras) in [
                           (15, 15), (10, 10)]],
                       ordens_de_servico=[OrdemDeServico(hoje - timedelta(days=dias_atras)) for dias_atras in
                                          [15, 15, 15, 14, 14, 13, 12, 12, 11, 5, 3, 1, 1]])
    polo_recife = Polo('Recife',
                       expedicoes=[Expedicao(quant, hoje - timedelta(days=dias_atras)) for (quant, dias_atras) in [
                           (30, 30), (15, 20), (15, 10)]],
                       ordens_de_servico=[OrdemDeServico(hoje - timedelta(days=dias_atras)) for dias_atras in [
                           30, 30, 30, 29, 29, 29, 29, 28, 28, 28, 28, 26, 26, 25, 25, 24, 24, 24, 24, 22, 22, 22,
                           18, 18, 18, 18, 18, 17, 16, 16, 16, 16, 15, 14, 14, 14, 11, 10, 10, 10, 9, 9, 9, 9, 8,
                           7, 7, 6, 6, 6, 5, 4, 4, 4, 3, 2, 1]])
    polo_sao_paulo = Polo('São Paulo',
                          expedicoes=[Expedicao(quant, hoje - timedelta(days=dias_atras)) for (quant, dias_atras) in [
                              (30, 60), (30, 55), (30, 50), (30, 45), (30, 40), (30, 35), (30, 30), (30, 10)]],
                          ordens_de_servico=[OrdemDeServico(hoje - timedelta(days=dias_atras)) for dias_atras in [
                              60, 60, 60, 59, 59, 58, 58, 58, 58, 57, 57, 57, 57, 57, 57, 56, 56, 56, 55, 55, 55, 55,
                              54, 54, 54, 54, 54, 53, 53, 53, 53, 52, 52, 52, 51, 51, 51, 51, 51, 51, 51, 49, 49, 49,
                              48, 48, 48, 48, 48, 47, 47, 47, 47, 47, 47, 47, 47, 46, 46, 46, 46, 46, 46, 46, 46, 45,
                              45, 45, 45, 45, 45, 43, 43, 43, 43, 43, 43, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 41,
                              41, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 39, 39, 39, 38, 38, 38, 38, 38, 38,
                              37, 37, 37, 37, 37, 37, 36, 36, 36, 36, 36, 35, 35, 35, 34, 34, 34, 33, 33, 33, 32, 31,
                              30, 30, 30, 29, 29, 29, 29, 28, 28, 28, 28, 26, 26, 25, 25, 24, 24, 24, 24, 22, 22, 22,
                              18, 18, 18, 18, 18, 17, 16, 16, 16, 16, 15, 14, 14, 14, 11, 10, 10, 10, 9, 9, 9, 9, 8,
                              7, 7, 6, 6, 6, 5, 4, 4, 4, 3, 2, 1]])
    polo_belo_horizonte = Polo('Belo Horizonte',
                               expedicoes=[Expedicao(quant, hoje - timedelta(days=dias_atras)) for (quant, dias_atras)
                                           in [(35, 50), (20, 40), (20, 30), (20, 10)]],
                               ordens_de_servico=[OrdemDeServico(hoje - timedelta(days=dias_atras)) for dias_atras in [
                                   49, 49, 48, 48, 48, 47, 47, 47, 47, 46, 46, 46, 46, 46, 45, 45, 45, 45, 45, 45, 43,
                                   43, 42, 42, 42, 42, 41, 41, 40, 40, 40, 40, 39, 39, 38, 38, 38, 37, 36, 36, 35, 34,
                                   33, 32, 31, 30, 30, 30, 29, 28, 26, 25, 24, 24, 24, 24, 22, 22, 22, 18, 18, 18, 18,
                                   18, 17, 16, 16, 16, 16, 15, 14, 14, 14, 11, 10, 10, 9, 9, 8, 7, 6, 6, 5, 4, 4, 3, 2,
                                   1]])
    test_session.add(polo_manaus)
    test_session.add(polo_recife)
    test_session.add(polo_sao_paulo)
    test_session.add(polo_belo_horizonte)
    ids = (polo_manaus.id_polo, polo_recife.id_polo, polo_sao_paulo.id_polo, polo_belo_horizonte.id_polo)
    test_session.commit()
    clear_mappers()

    yield ids

    if os.path.exists('test_database.sqlite'):
        os.remove('test_database.sqlite')
