from datetime import datetime
from uuid import uuid4

import pytest
from freezegun import freeze_time

from polo.model import OrdemDeServico, Expedicao, Polo
from polo.repository.orm import ordens_de_servico, expedicoes, polos


def test_ordens_de_servico_carrega_registros(in_memory_session):
    id_polo = uuid4()
    in_memory_session.execute(ordens_de_servico.insert(),
                              [
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 17)}
                              ])
    expected = [
        OrdemDeServico(datetime(2015, 12, 17)),
        OrdemDeServico(datetime(2015, 12, 17)),
        OrdemDeServico(datetime(2015, 12, 17))
    ]
    assert in_memory_session.query(OrdemDeServico).all() == expected


def test_expedicoes_carrega_registros(in_memory_session):
    id_polo = uuid4()
    in_memory_session.execute(expedicoes.insert(),
                              [
                                  {'id_polo': id_polo, 'num_terminais': 10, 'data_expedicao': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'num_terminais': 15, 'data_expedicao': datetime(2015, 12, 20)},
                                  {'id_polo': id_polo, 'num_terminais': 20, 'data_expedicao': datetime(2015, 12, 25)}
                              ])
    expected = [
        Expedicao(10, datetime(2015, 12, 17)),
        Expedicao(15, datetime(2015, 12, 20)),
        Expedicao(20, datetime(2015, 12, 25))
    ]
    assert in_memory_session.query(Expedicao).all() == expected


def test_polo_zerado_carrega_registros(in_memory_session):
    id_polo = uuid4()
    in_memory_session.execute(polos.insert(),
                              [
                                  {'id_polo': id_polo, 'nome': 'Polo Vazio', 'cobertura': 0}
                              ])
    expected = [
        Polo('Polo Vazio', id_polo)
    ]
    assert in_memory_session.query(Polo).all() == expected


@freeze_time("2015-12-25")
def test_polo_com_atendimentos_e_expedicoes_carrega_registros(in_memory_session):
    id_polo = uuid4()
    in_memory_session.execute(polos.insert(),
                              [
                                  {'id_polo': id_polo, 'nome': 'Polo Verde', 'cobertura': 0}
                              ])
    in_memory_session.execute(expedicoes.insert(),
                              [
                                  {'id_polo': id_polo, 'num_terminais': 10, 'data_expedicao': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'num_terminais': 5, 'data_expedicao': datetime(2015, 12, 20)},
                                  {'id_polo': id_polo, 'num_terminais': 5, 'data_expedicao': datetime(2015, 12, 25)}
                              ])
    in_memory_session.execute(ordens_de_servico.insert(),
                              [
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 17)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 18)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 20)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 20)},
                                  {'id_polo': id_polo, 'data_atendimento': datetime(2015, 12, 20)}
                              ])
    new_polo = in_memory_session.query(Polo).one()

    assert new_polo.id_polo == id_polo
    assert new_polo.nome == 'Polo Verde'
    assert len(new_polo._terminais_recebidos) == 3
    assert len(new_polo._ordens_executadas) == 7


def test_polo_vazio_persiste(in_memory_session):
    polo = Polo('Novo Polo')
    in_memory_session.add(polo)
    in_memory_session.commit()

    polo_recuperado = in_memory_session.query(Polo).one()
    assert polo == polo_recuperado


@freeze_time("2015-12-25")
def test_polo_operacional_persiste(in_memory_session):
    polo = Polo('Novo Polo',
                expedicoes=[
                    Expedicao(10, datetime(2015, 12, 17)),
                    Expedicao(6, datetime(2015, 12, 20))],
                ordens_de_servico=[
                    OrdemDeServico(datetime(2015, 12, 17)),
                    OrdemDeServico(datetime(2015, 12, 17)),
                    OrdemDeServico(datetime(2015, 12, 18)),
                    OrdemDeServico(datetime(2015, 12, 19)),
                    OrdemDeServico(datetime(2015, 12, 22)),
                ])
    in_memory_session.add(polo)
    in_memory_session.commit()

    polo_recuperado = in_memory_session.query(Polo).one()
    assert polo == polo_recuperado
    assert len(polo_recuperado._ordens_executadas) == 5
    assert len(polo_recuperado._terminais_recebidos) == 2
    assert polo_recuperado.cobertura == 17
