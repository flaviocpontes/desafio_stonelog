from datetime import datetime, timedelta, date

import pytest
from freezegun import freeze_time

from polo.helpers import remove_intraday
from polo.model import Polo, Expedicao, OrdemDeServico


def test_mesmo_polos_diferentes_mesmo_nome():
    polo1 = Polo("Mesmo nome")
    polo2 = Polo("Mesmo nome")

    assert polo1 != polo2


def test_expedicao_decrementa_estoque():
    polo1 = Polo("Mesmo nome", expedicoes=[Expedicao(1, remove_intraday(datetime.today() - timedelta(days=1)))])
    polo1.os_executada(OrdemDeServico(remove_intraday(datetime.today())))
    assert polo1.terminais_disponiveis == 0


def test_polo_e_diferente_de_outros_objetos():
    polo1 = Polo("Mesmo nome")
    lista = [1, 2, 3, 4, 5]

    assert polo1 != lista


def test_polo_vazio_cobertura_vermelha_0():
    polo = Polo("Polo Vermelho")

    assert polo.cobertura == 0
    assert polo.criticidade == 'VERMELHA'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_amarela_10_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(11, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 10
    assert polo.criticidade == 'AMARELA'


@freeze_time("2015-12-15")
def test_polo_operacional_indica_data_atual_inconsistente(in_memory_session):
    with pytest.raises(RuntimeError):
        polo = Polo('Novo Polo',
                    expedicoes=[
                        Expedicao(10, datetime(2015, 12, 17))],
                    ordens_de_servico=[
                        OrdemDeServico(datetime(2015, 12, 17)),
                        OrdemDeServico(datetime(2015, 12, 17)),
                    ])


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_amarela_13_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(14, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 13
    assert polo.criticidade == 'AMARELA'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_verde_14_dias():
    polo = Polo("Polo Verde")
    polo.terminais_recebidos(Expedicao(15, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 14
    assert polo.criticidade == 'VERDE'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_verde_18_dias():
    polo = Polo("Polo Verde")
    polo.terminais_recebidos(Expedicao(19, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 18
    assert polo.criticidade == 'VERDE'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_amarela_19_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(20, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 19
    assert polo.criticidade == 'AMARELA'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_amarela_23_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(24, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 23
    assert polo.criticidade == 'AMARELA'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_criticidade_vermelha_24_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(25, datetime(2020, 7, 16)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 24
    assert polo.criticidade == 'VERMELHA'


@freeze_time("2020-07-17")
def test_polo_1_atendimento_anteontem_criticidade_vermelha_8_dias():
    polo = Polo("Polo Vermelho")
    polo.terminais_recebidos(Expedicao(5, datetime(2020, 7, 15)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 15)))

    assert polo.cobertura == 8
    assert polo.criticidade == 'VERMELHA'


@freeze_time("2020-07-17")
def test_polo_2_atendimentos_media_1_criticidade_vermelha_8_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(11, datetime(2020, 7, 15)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 15)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 9
    assert polo.criticidade == 'VERMELHA'


@freeze_time("2020-07-17")
def test_polo_5_atendimentos_2_expedicoes_media_1_criticidade_vermelha_2_dias():
    polo = Polo("Polo Amarelo")
    polo.terminais_recebidos(Expedicao(3, datetime(2020, 7, 10)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 11)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 12)))
    polo.terminais_recebidos(Expedicao(8, datetime(2020, 7, 13)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 14)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 15)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 7
    assert polo.criticidade == 'VERMELHA'


@freeze_time("2020-07-17")
def test_multiplos_atendimentos():
    polo = Polo("Polo Verde")
    polo.terminais_recebidos(Expedicao(3, datetime(2020, 7, 5)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 5)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 8)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 8)))
    polo.terminais_recebidos(Expedicao(7, datetime(2020, 7, 10)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 12)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 15)))
    polo.terminais_recebidos(Expedicao(4, datetime(2020, 7, 10)))
    polo.os_executada(OrdemDeServico(datetime(2020, 7, 16)))

    assert polo.cobertura == 16
    assert polo.criticidade == 'VERDE'
