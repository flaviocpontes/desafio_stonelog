from datetime import datetime

from polo.helpers import remove_intraday


def test_remove_intraday():
    today = datetime.today()
    today = remove_intraday(today)
    assert today.hour == 0
    assert today.minute == 0
    assert today.second == 0
    assert today.microsecond == 0