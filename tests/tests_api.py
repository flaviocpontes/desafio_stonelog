from datetime import date, timedelta
from uuid import uuid4

from freezegun import freeze_time

from config import TestConfig
from polo.app import create_app


def test_welcome_endpoint(mock_data):
    app = create_app(TestConfig)
    client = app.test_client()
    res = client.get('/')
    assert res.json == {
        'mensagem': 'Bem-vindo à API',
        'uri': 'http://localhost/api/polos'
    }


def test_api_consegue_listar_polos(mock_data):
    app = create_app(TestConfig)
    client = app.test_client()
    res = client.get('/api/polos')
    assert res.json == {
        'polos': [
            {'nome': 'Recife', 'id_polo': str(mock_data[1]), 'cobertura': 1, 'criticidade': 'VERMELHA',
             'uri': f'http://localhost/api/polos/{str(mock_data[1])}'},
            {'nome': 'Belo Horizonte', 'id_polo': str(mock_data[3]), 'cobertura': 3, 'criticidade': 'VERMELHA',
             'uri': f'http://localhost/api/polos/{str(mock_data[3])}'},
            {'nome': 'Manaus', 'id_polo': str(mock_data[0]), 'cobertura': 13, 'criticidade': 'AMARELA',
             'uri': f'http://localhost/api/polos/{str(mock_data[0])}'},
            {'nome': 'São Paulo', 'id_polo': str(mock_data[2]), 'cobertura': 16, 'criticidade': 'VERDE',
             'uri': f'http://localhost/api/polos/{str(mock_data[2])}'},
        ]}


def test_api_consegue_detalhar_polo(mock_data):
    hoje = date.today()

    app = create_app(TestConfig)
    client = app.test_client()
    res = client.get(f'/api/polos/{mock_data[0]}')
    assert res.json == {'nome': 'Manaus',
                        'id_polo': str(mock_data[0]),
                        'cobertura': 13,
                        'criticidade': 'AMARELA',
                        'ordens_de_servico': [(hoje - timedelta(days=dias)).isoformat() for dias in
                                              [15, 15, 15, 14, 14, 13, 12, 12, 11, 5, 3, 1, 1]],
                        'expedicoes': [{'quantidade': quant, 'data': (hoje - timedelta(days=dias)).isoformat()} for
                                       (quant, dias) in [
                                           (15, 15), (10, 10)]],
                        'terminais_disponiveis': 12,
                        'media_atendimentos': 0.87
                        }


def test_expede_aparelhos(mock_data):
    hoje = date.today()

    app = create_app(TestConfig)
    client = app.test_client()
    res = client.post(f'/api/polos/{mock_data[0]}/expedicao',
                      json={'quantidade': 3}, follow_redirects=True)

    assert res.json == {'expedicao': {'quantidade': 3},
                        'polo': {'nome': 'Manaus',
                                 'id_polo': str(mock_data[0]),
                                 'cobertura': 17,
                                 'criticidade': 'VERDE',
                                 'ordens_de_servico': [(hoje - timedelta(days=dias)).isoformat() for dias in
                                                       [15, 15, 15, 14, 14, 13, 12, 12, 11, 5, 3, 1, 1]],
                                 'expedicoes': [{'quantidade': quant, 'data': (hoje - timedelta(days=dias)).isoformat()}
                                                for
                                                (quant, dias) in [
                                                    (15, 15), (10, 10), (3, 0)]],
                                 'terminais_disponiveis': 15,
                                 'media_atendimentos': 0.87
                                 }}


@freeze_time("2020-07-23T19:08:27.000000")
def test_formato_incorreto_inteiro_negativo(mock_data):
    app = create_app(TestConfig)
    client = app.test_client()
    res = client.post(f'/api/polos/{mock_data[0]}/expedicao', json={'quantidade': -10}, follow_redirects=True)
    assert res.status == '400 BAD REQUEST'
    assert res.json == {'message': 'Formato incorreto. O payload tem que seguir o formato '
                                   '{"quantidade": <inteiro positivo>}',
                        'path': f'/api/polos/{mock_data[0]}/expedicao',
                        'payload': {'quantidade': -10},
                        'status_code': 400,
                        'timestamp': '2020-07-23T19:08:27'}


@freeze_time("2020-07-23T19:08:27.000000")
def test_formato_incorreto_string(mock_data):
    app = create_app(TestConfig)
    client = app.test_client()
    res = client.post(f'/api/polos/{mock_data[0]}/expedicao', json={'quantidade': '255'}, follow_redirects=True)
    assert res.status == '400 BAD REQUEST'
    assert res.json == {'message': 'Formato incorreto. O payload tem que seguir o formato '
                                   '{"quantidade": <inteiro positivo>}',
                        'path': f'/api/polos/{mock_data[0]}/expedicao',
                        'payload': {'quantidade': '255'},
                        'status_code': 400,
                        'timestamp': '2020-07-23T19:08:27'}


@freeze_time("2020-07-23T19:08:27.000000")
def test_formato_incorreto_chave_errada(mock_data):
    app = create_app(TestConfig)
    client = app.test_client()
    res = client.post(f'/api/polos/{mock_data[0]}/expedicao', json={'quant': 255}, follow_redirects=True)
    assert res.status == '400 BAD REQUEST'
    assert res.json == {'message': 'Formato incorreto. O payload tem que seguir o formato '
                                   '{"quantidade": <inteiro positivo>}',
                        'path': f'/api/polos/{mock_data[0]}/expedicao',
                        'payload': {'quant': 255},
                        'status_code': 400,
                        'timestamp': '2020-07-23T19:08:27'}


@freeze_time("2020-07-23T19:08:27.000000")
def test_formato_incorreto_payload_vazio(mock_data):
    app = create_app(TestConfig)
    client = app.test_client()
    res = client.post(f'/api/polos/{mock_data[0]}/expedicao', json={}, follow_redirects=True)
    assert res.status == '400 BAD REQUEST'
    assert res.json == {'message': 'Formato incorreto. O payload tem que seguir o formato '
                                   '{"quantidade": <inteiro positivo>}',
                        'path': f'/api/polos/{mock_data[0]}/expedicao',
                        'payload': {},
                        'status_code': 400,
                        'timestamp': '2020-07-23T19:08:27'}


@freeze_time("2020-07-23T19:08:27.000000")
def test_polo_inexistente(mock_data):
    hoje = date.today()
    id_polo = uuid4()

    app = create_app(TestConfig)
    client = app.test_client()
    res = client.get(f'/api/polos/{id_polo}')
    assert res.status == '404 NOT FOUND'
    assert res.json == {'message': f'O polo {id_polo} não pode ser encontrado',
                        'path': f'/api/polos/{id_polo}',
                        'status_code': 404,
                        'timestamp': '2020-07-23T19:08:27'}
